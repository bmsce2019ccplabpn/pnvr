#include<stdio.h>
void input(int *,int *);
void swap(int *,int *);
int main()
{
    int x,y;
    input(&x,&y);
    printf("before swaping a=%d  b=%d\n",x,y);
    swap(&x,&y);
    printf("after swaping a=%d  b=%d\n",x,y);
    return 0;
}
void input(int *a,int *b)
{
    printf("enter numbers\n");
    scanf("%d%d",a,b);
}
void swap(int *a,int *b)
{
    int t;
    t=*a;
    *a=*b;
    *b=t;
}