#include <stdio.h>
int main()
{
   int a[10], x, i, n, y;

   printf("Enter number of elements in a\n");
   scanf("%d", &n);

   printf("Enter %d elements\n", n);

   for (i = 0; i < n; i++)
      scanf("%d", &a[i]);

   printf("Enter the location to insert an element\n");
   scanf("%d", &x);

   printf("Enter the value to insert\n");
   scanf("%d", &y);

   for (i = n - 1; i >= x - 1; i--)
        a[i+1] = a[i];

   a[x-1] = y;

   printf("Resultant a is\n");

   for (i = 0; i <= n; i++)
      printf("%d\n", a[i]);

   return 0;
}