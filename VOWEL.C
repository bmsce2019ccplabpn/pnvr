#include <stdio.h>
char input();
int check(char);
void output(int);

int main()
{    
    char a;
    int b;
	printf("vowel consonant checking\n");
    a=input();
    b=check(a);
    output(b);
    return 0;
}
char input()
{
    char a;
    printf("enter a alphabet\n");
    scanf("%c",&a);
    return a;
}
int check(char a)
{
    int b;
    switch(a)
    {
        case'A':
        case'a':
        case'B':
        case'b':
        case'C':
        case'c':
        case'D':
        case'd':
        case'E':
        case'e':
            b=1;
            break;
         default:
            b=0;   
    }
    return b;
}
void output(int b)
{
    if(b==1)
        printf("its an vowel\n");
    else  
         printf("its a consonant\n"); 
}
        