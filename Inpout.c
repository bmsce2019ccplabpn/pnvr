#include <stdio.h>

int main()
{
    
    int i, num;
    printf("Enter size of array: \n");
    scanf("%d", &num);
    int arr[num];

    printf("Enter %d elements in the array : \n", num);

    
    for(i=0; i<num; i++)
    {
        scanf("%d",&arr[i]);
    }

    
    printf("\nElements in array are: \n");
    for(i=0; i<num; i++)
    {
        printf("%d=%d\n",i,arr[i]);
    }

    return 0;
}
