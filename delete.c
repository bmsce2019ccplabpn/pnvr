#include <stdio.h>

int main()
{
   int a[10], x, i, n;

   printf("Enter number of elements in array\n");
   scanf("%d", &n);

   printf("Enter %d elements\n", n);

   for (i = 0; i < n; i++)
      scanf("%d", &a[i]);

   printf("Enter the location which you want to delete\n");
   scanf("%d", &x);

   if (x>= n+1)
      printf("Deletion not possible.\n");
   else
   {
      for (i = x - 1; i < n - 1; i++)
         a[i] = a[i+1];

      printf("Resultant array\n");

      for (i = 0; i < n - 1; i++)
         printf("%d\n", a[i]);
   }

   return 0;
}
